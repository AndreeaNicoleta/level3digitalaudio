#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>
#include <qvector.h>

namespace Ui {
	class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit DSPMainWindow(QWidget *parent = 0);
	~DSPMainWindow();
	QVector<double> ftj(QVector<double> yAxis, double frequency);
	QVector<double> modulated(double valFrequency, QVector<double> xAxis);
	QVector<double> carrier(double valFrequency, QVector<double> xAxis);
	private slots:
	void mf_OkButton();
	double samplingRateShannon(QVector<double> xAxis);
	double sinc(double x);
private:
	Ui::DSPMainWindow *ui;
};

#endif // DSPMAINWINDOW_H
