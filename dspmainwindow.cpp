#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>
#include "aquila/source/generator/TriangleGenerator.h"
#include<aquila\source\generator\SineGenerator.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::DSPMainWindow)
{
	ui->setupUi(this);
	QObject::connect(ui->pushButton, SIGNAL(released()), this, SLOT(mf_OkButton()));

}

DSPMainWindow::~DSPMainWindow()
{
	delete ui;
}

QVector<double>DSPMainWindow::modulated(double valFrequency, QVector<double> xAxis)
{
	QVector<double>yAxis(44100);
	Aquila::TriangleGenerator generator(samplingRateShannon(xAxis));
	generator.setFrequency(valFrequency).setAmplitude(255).generate(samplingRateShannon(xAxis) * 0.01);
	for (std::size_t i = 0; i < generator.getSamplesCount(); ++i)
	{
		//generator.sample(i) este valoarea sample-ului la pozitia i
		yAxis[i] = generator.sample(i)* sin(valFrequency * 2 * M_PI);
		//m(t)=M*sin(omega*m + fi);
	}

	return yAxis;
}
QVector<double>DSPMainWindow::carrier(double valFrequency, QVector<double> xAxis)
{
	valFrequency *= 100;
	QVector<double>yAxis(44100);

	Aquila::SineGenerator generator(samplingRateShannon(xAxis));
	generator.setFrequency(valFrequency).setAmplitude(255).generate(samplingRateShannon(xAxis) * 0.01);
	// simple index-based iteration
	for (std::size_t i = 0; i < generator.getSamplesCount(); ++i)
	{
		//generator.sample(i) este valoarea sample-ului la pozitia i
		yAxis[i] = generator.sample(i)* sin(valFrequency * 2 * M_PI);
		//formula am luat-o de pe acest link http://www.radio-electronics.com/info/rf-technology-design/am-amplitude-modulation/theory-equations.php
		//C(t)=C*sin(omega*c + fi);
	}

	return yAxis;
}

double DSPMainWindow::samplingRateShannon(QVector<double> xAxis)
{
	//sampling rate using Nyquist�Shannon
	double N = 100;
	double T = 5000;
	double result = 0;
	for (int i = 0; i < 44100; i++)
	{
		result += (N + 1)*sinc((N + 1)*xAxis[i] / T) - N*sinc(N*xAxis[i] / T);
	}
	return result;
	
}

double DSPMainWindow::sinc(double x)
{
	return sin(x)/x;
}



void DSPMainWindow::mf_OkButton()
{
	QVector<double> xAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}
	QVector<double>yAxis1(44100);
	QVector<double>yAxis2(44100);
	QVector<double>yAxisTotal(44100);
	yAxis1 = modulated(ui->spinBox->value(), xAxis);
	yAxis2 = carrier(ui->spinBox->value(), xAxis);

	for (int i = 0; i < 44100; i++)
	{
		yAxisTotal[i] = (1 + yAxis1[i]) * yAxis2[i];
	}
	
	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxisTotal);
	ui->plot->xAxis->setRange(0, 0.01);
	ui->plot->yAxis->setRange(-1, 1);
	ui->plot->replot();

}
